DEPLOY_USER ?= deploy
DEPLOY_HOST ?= 137.226.248.165
DEPLOY_PATH ?= /mnt/nfs/jhub-materials-pvc-101eb8f0-75b8-11e9-a5ea-fa163e541365

deploy:
	rsync -a --progress --delete . $(DEPLOY_USER)@$(DEPLOY_HOST):$(DEPLOY_PATH)/
	ssh $(DEPLOY_USER)@$(DEPLOY_HOST) chown -R $(DEPLOY_USER): $(DEPLOY_PATH)
	ssh $(DEPLOY_USER)@$(DEPLOY_HOST) chmod go-w -R  $(DEPLOY_PATH)
	ssh $(DEPLOY_USER)@$(DEPLOY_HOST) rm $(DEPLOY_PATH)/Makefile
